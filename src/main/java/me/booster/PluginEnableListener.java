package me.booster;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;

public class PluginEnableListener implements Listener {

    private final BoosterMain plugin;

    public PluginEnableListener(BoosterMain plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPluginEnable(PluginEnableEvent event) {
        if (event.getPlugin().equals(plugin)) {
            for (Player player : Bukkit.getOnlinePlayers()) {
                MySQL mysql = BoosterMain.getInstance().getMySQL();
                int remaining = mysql.getCountdown(player.getUniqueId(), "SpeedBooster");

                if (remaining > 0) {
                    player.setWalkSpeed(0.5f);
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            int remaining = mysql.getCountdown(player.getUniqueId(), "SpeedBooster");
                            if (remaining <= 0) {
                                player.setWalkSpeed(0.2f);
                                player.sendMessage(getColoredString(BoosterMain.getInstance().getConfig().getString("SpeedBoosterDeaktiviertNachricht")));
                                this.cancel();
                            } else {
                                mysql.saveCountdown(player.getUniqueId(), "SpeedBooster", remaining - 1);
                            }
                        }
                    }.runTaskTimer(plugin, 0, 20); // 20 ticks = 1 second
                }
                // Handle FlyBooster
                int flyRemaining = mysql.getCountdown(player.getUniqueId(), "FlyBooster");
                if (flyRemaining > 0) {
                    player.setAllowFlight(true);
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            int remaining = mysql.getCountdown(player.getUniqueId(), "FlyBooster");
                            if (remaining <= 0) {
                                player.setAllowFlight(false);
                                player.sendMessage(getColoredString(BoosterMain.getInstance().getConfig().getString("FlyBoosterDeaktiviertNachricht")));
                                this.cancel();
                            } else {
                                mysql.saveCountdown(player.getUniqueId(), "FlyBooster", remaining - 1);
                            }
                        }
                    }.runTaskTimer(plugin, 0, 20);
                }
                int jumpRemaining = mysql.getCountdown(player.getUniqueId(), "JumpBooster");
                if (jumpRemaining > 0) {
                    player.setMetadata("JumpBooster", new FixedMetadataValue(BoosterMain.getPlugin(BoosterMain.class), true));
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            int remaining = mysql.getCountdown(player.getUniqueId(), "JumpBooster");
                            if (remaining <= 0) {
                                player.removeMetadata("JumpBooster", BoosterMain.getPlugin(BoosterMain.class));
                                player.sendMessage(getColoredString(BoosterMain.getInstance().getConfig().getString("JumpBoosterDeaktiviertNachricht")));
                                this.cancel();
                            } else {
                                mysql.saveCountdown(player.getUniqueId(), "JumpBooster", remaining - 1);
                            }
                        }
                    }.runTaskTimer(plugin, 0, 20);
                }
                int boosterBombeRemaining = mysql.getCountdown(player.getUniqueId(), "BoosterBombe");
                if (boosterBombeRemaining > 0) {
                    player.setWalkSpeed(0.5f);
                    player.setMetadata("JumpBooster", new FixedMetadataValue(plugin, true));
                    player.setAllowFlight(true);
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            int remaining = mysql.getCountdown(player.getUniqueId(), "BoosterBombe");
                            if (remaining <= 0) {
                                player.setWalkSpeed(0.2f);
                                player.removeMetadata("JumpBooster", plugin);
                                player.setAllowFlight(false);
                                player.sendMessage(getColoredString(BoosterMain.getInstance().getConfig().getString("BoosterBombeDeaktiviertNachricht")));
                                this.cancel();
                            } else {
                                mysql.saveCountdown(player.getUniqueId(), "BoosterBombe", remaining - 1);
                            }
                        }
                    }.runTaskTimer(plugin, 0, 20);
                }
            }
        }
    }
    private String getColoredString(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }
}