package me.booster;

import me.booster.GUI.Menuwahl;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Commands implements CommandExecutor {

    private final MySQL mysql;
    private final JavaPlugin plugin;

    public Commands(MySQL mysql, JavaPlugin plugin) {
        this.mysql = mysql;
        this.plugin = plugin;
    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            Menuwahl menuwahl = new Menuwahl(mysql, plugin);
            menuwahl.openMenu(player);
            return true;
        }
        return false;
    }
}
