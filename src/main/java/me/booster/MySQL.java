package me.booster;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.*;
import java.util.UUID;

public class MySQL {

    private Connection connection;
    private final JavaPlugin plugin;


    public MySQL(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    public JavaPlugin getPlugin() {
        return this.plugin;
    }

    public void connect() {
        try {
            if (connection == null || connection.isClosed()) {
                String url = plugin.getConfig().getString("Datenbank.url");
                String username = plugin.getConfig().getString("Datenbank.username");
                String password = plugin.getConfig().getString("Datenbank.password");

                connection = DriverManager.getConnection(url, username, password);
                plugin.getServer().getConsoleSender().sendMessage(getColoredString(plugin.getConfig().getString("VerbindungAktiv")));
            }
        } catch (SQLException e) {
            plugin.getServer().getConsoleSender().sendMessage(getColoredString(plugin.getConfig().getString("VerbindungFehlgeschlagen")));
            e.printStackTrace();
        }
    }


    public Connection getConnection() {
        return connection;
    }

    public boolean isConnected() {
        try {
            return connection != null && !connection.isClosed();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }


    public void saveCountdown(UUID uuid, String booster, int countdown) {
        try {
            if (connection == null || connection.isClosed()) {
                connect();
            }

            String query = "INSERT INTO countdowns (uuid, booster, countdown) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE countdown = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, uuid.toString());
            preparedStatement.setString(2, booster);
            preparedStatement.setInt(3, countdown);
            preparedStatement.setInt(4, countdown);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getCountdown(UUID uuid, String booster) {
        try {
            if (connection == null || connection.isClosed()) {
                connect();
            }

            String query = "SELECT countdown FROM countdowns WHERE uuid = ? AND booster = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, uuid.toString());
            preparedStatement.setString(2, booster);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt("countdown");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public void addAmountColumn() {
        try {
            if (connection == null || connection.isClosed()) {
                connect();
            }

            DatabaseMetaData dbm = connection.getMetaData();
            ResultSet tables = dbm.getColumns(null, null, "countdowns", "amount");
            if (!tables.next()) {
                String query = "ALTER TABLE countdowns ADD COLUMN amount INT DEFAULT 0";
                PreparedStatement preparedStatement = connection.prepareStatement(query);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getAmount(String playerId, String boosterType) {
        try {
            PreparedStatement ps = getConnection().prepareStatement("SELECT amount FROM countdowns WHERE uuid = ? AND booster = ?");
            ps.setString(1, playerId);
            ps.setString(2, boosterType);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getInt("amount");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void setAmount(String uuid, String booster, int amount) {
        try {
            PreparedStatement ps = getConnection().prepareStatement("UPDATE countdowns SET amount = ? WHERE UUID = ? AND booster = ?");
            ps.setInt(1, amount);
            ps.setString(2, uuid);
            ps.setString(3, booster);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void incrementAmount(String uuid, String booster) {
        int currentAmount = getAmount(uuid, booster);
        setAmount(uuid, booster, currentAmount + 1);
    }


    private String getColoredString(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }

}