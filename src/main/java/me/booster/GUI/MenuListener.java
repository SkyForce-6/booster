package me.booster.GUI;

import me.booster.BoosterMain;
import me.booster.MySQL;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class MenuListener implements Listener {

    private final MySQL mysql;
    private final JavaPlugin plugin;

    public MenuListener(JavaPlugin plugin) {
        this.plugin = plugin;
        this.mysql = BoosterMain.getInstance().getMySQL();
    }

    @EventHandler
    public void onMenuClick(InventoryClickEvent event) {
        if (event.getCurrentItem() == null || !event.getCurrentItem().hasItemMeta()) {
            return;
        }

        if (event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes('&', BoosterMain.getInstance().getConfig().getString("GUI.Back.Name")))) {
            Menuwahl menuwahl = new Menuwahl(mysql, plugin);
            menuwahl.openMenu((Player) event.getWhoClicked());
        }

        Player player = (Player) event.getWhoClicked();
        String[] boosters = {"SpeedBooster", "JumpBooster", "FlyBooster", "BoosterBombe"};

        for (String booster : boosters) {
            if (event.getView().getTitle().equals(getColoredString(BoosterMain.getInstance().getConfig().getString("MenuName"))) && event.getCurrentItem().getItemMeta().getDisplayName().equals(getColoredString(BoosterMain.getInstance().getConfig().getString("GUI." + booster + ".Name")))) {
                event.setCancelled(true);

                if (isBoosterBombActive(player) && !booster.equals("BoosterBombe")) {
                    player.sendMessage(getColoredString(BoosterMain.getInstance().getConfig().getString("BoosterBombeAktiv")));
                    return;
                }

                int amount = mysql.getAmount(player.getUniqueId().toString(), booster);
                if (amount >= 1) {
                    mysql.setAmount(player.getUniqueId().toString(), booster, amount - 1);

                } else {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', BoosterMain.getInstance().getConfig().getString( "KeinBoosterNachricht")));
                    event.getWhoClicked().closeInventory();
                    return;
                }
                if (amount == 0) {
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', BoosterMain.getInstance().getConfig().getString( "KeinBoosterNachricht")));
                    event.getWhoClicked().closeInventory();
                    return;
                }

                int remaining = mysql.getCountdown(player.getUniqueId(), booster);

                if (remaining > 0) {
                    player.sendMessage(getColoredString(BoosterMain.getInstance().getConfig().getString(booster + "IstAktiv")));
                    event.getWhoClicked().closeInventory();
                    return;
                }

                int countdown = BoosterMain.getInstance().getConfig().getInt(booster + "Zeit");
                mysql.saveCountdown(player.getUniqueId(), booster, countdown);

                switch (booster) {
                    case "SpeedBooster":
                        player.setWalkSpeed(0.5f);
                        break;
                    case "JumpBooster":
                        player.setMetadata("JumpBooster", new FixedMetadataValue(plugin, true));
                        break;
                    case "FlyBooster":
                        player.setAllowFlight(true);
                        break;
                    case "BoosterBombe":
                        player.setWalkSpeed(0.5f);
                        player.setMetadata("JumpBooster", new FixedMetadataValue(plugin, true));
                        player.setAllowFlight(true);
                        break;
                }

                player.sendMessage(getColoredString(BoosterMain.getInstance().getConfig().getString(booster + "Nachricht")));
                event.getWhoClicked().closeInventory();

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        int remaining = mysql.getCountdown(player.getUniqueId(), booster);
                        if (remaining <= 0) {
                            switch (booster) {
                                case "SpeedBooster":
                                    player.setWalkSpeed(0.2f);
                                    break;
                                case "JumpBooster":
                                    player.removeMetadata("JumpBooster", plugin);
                                    break;
                                case "FlyBooster":
                                    player.setAllowFlight(false);
                                    break;
                                case "BoosterBombe":
                                    player.setWalkSpeed(0.2f);
                                    player.removeMetadata("JumpBooster", plugin);
                                    player.setAllowFlight(false);
                                    break;
                            }

                            player.sendMessage(getColoredString(BoosterMain.getInstance().getConfig().getString(booster + "DeaktiviertNachricht")));
                            mysql.saveCountdown(player.getUniqueId(), booster, 0);
                            this.cancel();
                        } else {
                            mysql.saveCountdown(player.getUniqueId(), booster, remaining - 1);
                        }
                    }
                }.runTaskTimer(mysql.getPlugin(), 0, 20);
            }
        }
    }

    private boolean isBoosterBombActive(Player player) {
        return mysql.getCountdown(player.getUniqueId(), "BoosterBombe") > 0;
    }

    private String getColoredString(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }
}