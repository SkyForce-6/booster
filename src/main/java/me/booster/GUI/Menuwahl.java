package me.booster.GUI;

import me.booster.ApiUsage.VaultGUI;
import me.booster.BoosterMain;
import me.booster.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Menuwahl implements Listener {

    private final MySQL mysql;
    private final JavaPlugin plugin;

    public Menuwahl(MySQL mysql, JavaPlugin plugin) {
        this.mysql = mysql;
        this.plugin = plugin;
    }



    public void createMenu(Player player) {

        Inventory gui = Bukkit.createInventory(null, 5 * 9, "§cBooster");
        ItemStack boosterItem = new ItemStack(Material.EMERALD, 1);
        ItemMeta boosterMeta = boosterItem.getItemMeta();
        boosterMeta.setDisplayName("§aBooster");
        boosterItem.setItemMeta(boosterMeta);
        gui.setItem(20, boosterItem);


        ItemStack shopItem = new ItemStack(Material.SUNFLOWER, 1);
        ItemMeta shopMeta = shopItem.getItemMeta();
        shopMeta.setDisplayName("§aBooster Shop");
        shopItem.setItemMeta(shopMeta);
        gui.setItem(24, shopItem);
        player.openInventory(gui);


        ItemStack shopItem1 = new ItemStack(Material.BARRIER, 1);
        ItemMeta shopMeta1 = shopItem1.getItemMeta();
        shopMeta1.setDisplayName("§cSchließen");
        shopItem1.setItemMeta(shopMeta1);
        gui.setItem(22, shopItem1);


        player.openInventory(gui);
    }

    public void openMenu(Player player) {
        createMenu(player);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getCurrentItem() == null || !event.getCurrentItem().hasItemMeta()) {
            return;
        }

        Player player = (Player) event.getWhoClicked();
        ItemStack clickedItem = event.getCurrentItem();

        if (event.getView().getTitle().equals("§cBooster")) {
            event.setCancelled(true);

            if (clickedItem.getType() == Material.EMERALD && clickedItem.getItemMeta().getDisplayName().equals("§aBooster")) {
                // Öffnen Sie das Hauptmenü, wenn der Smaragd angeklickt wird
                MainMenu mainMenu = new MainMenu(mysql);
                mainMenu.openMenu(player);
            } else if (clickedItem.getType() == Material.SUNFLOWER && clickedItem.getItemMeta().getDisplayName().equals("§aBooster Shop")) {
                // Öffnen Sie das VaultGUI, wenn die Sonnenblume angeklickt wird
                VaultGUI vaultGUI = BoosterMain.getVaultGUI();
                if (vaultGUI != null) {
                    vaultGUI.openMenu(player);
                }
            }else if (clickedItem.getType() == Material.BARRIER && clickedItem.getItemMeta().getDisplayName().equals("§cSchließen")) {
                player.closeInventory();
            }
        }
    }
}