package me.booster.GUI;

import me.booster.BoosterMain;
import me.booster.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import java.util.Collections;
import java.util.List;

public class MainMenu {

    private final MySQL mysql;

    public MainMenu(MySQL mysql) {
        this.mysql = mysql;
    }

    public void openMenu(Player player) {
        Inventory menu = createMenu(player);
        player.openInventory(menu);
        Bukkit.getScheduler().runTaskLater(BoosterMain.getInstance(), player::updateInventory, 1L);
    }

    public Inventory createMenu(Player player) {
        Inventory menu = Bukkit.createInventory(null, 9, getColoredString(BoosterMain.getInstance().getConfig().getString("MenuName")));

        // SpeedBooster
        createMenuItem(menu, "SpeedBooster", 0, player);

        // FlyBooster
        createMenuItem(menu, "FlyBooster", 1, player);

        // JumpBooster
        createMenuItem(menu, "JumpBooster", 2, player);

        // BoosterBombe
        createMenuItem(menu, "BoosterBombe", 3, player);

        createBackItem(menu, 8);

        return menu;
    }

    private void createBackItem(Inventory menu, int slot) {
        String itemName = BoosterMain.getInstance().getConfig().getString("GUI.Back.Item");
        if (itemName != null) {
            Material material = Material.getMaterial(itemName);
            if (material != null) {
                ItemStack backItem = new ItemStack(material);
                ItemMeta meta = backItem.getItemMeta();


                String displayName = BoosterMain.getInstance().getConfig().getString("GUI.Back.Name");
                if (displayName != null) {
                    meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
                }


                List<String> lore = BoosterMain.getInstance().getConfig().getStringList("GUI.Back.Lore");
                if (lore != null && !lore.isEmpty()) {
                    for (int i = 0; i < lore.size(); i++) {
                        lore.set(i, ChatColor.translateAlternateColorCodes('&', lore.get(i)));
                    }
                    meta.setLore(lore);
                }
                backItem.setItemMeta(meta);

                menu.setItem(slot, backItem);
            }
        }
    }
    private void createMenuItem(Inventory menu, String boosterName, int slot, Player player) {


        String itemName = BoosterMain.getInstance().getConfig().getString("GUI." + boosterName + ".Item");
        if (itemName != null) {
            Material material = Material.getMaterial(itemName);
            if (material != null) {
                ItemStack boosterItem = new ItemStack(material);
                ItemMeta meta = boosterItem.getItemMeta();

                String displayName = BoosterMain.getInstance().getConfig().getString("GUI." + boosterName + ".Name");
                if (displayName != null) {
                    meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
                }

                List<String> lore = BoosterMain.getInstance().getConfig().getStringList("GUI." + boosterName + ".Lore");
                if (lore != null && !lore.isEmpty()) {
                    for (int i = 0; i < lore.size(); i++) {
                        if (lore.get(i).contains("%anzahl%")) {
                            int amount = mysql.getAmount(player.getUniqueId().toString(), boosterName);
                            lore.set(i, ChatColor.translateAlternateColorCodes('&', lore.get(i).replace("%anzahl%", String.valueOf(amount))));
                        } else {
                            lore.set(i, ChatColor.translateAlternateColorCodes('&', lore.get(i)));
                        }
                    }
                    meta.setLore(lore);
                }
                boosterItem.setItemMeta(meta);

                menu.setItem(slot, boosterItem);

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        int remaining = mysql.getCountdown(player.getUniqueId(), boosterName);
                        ItemStack boosterItem = menu.getItem(slot);
                        ItemMeta meta = boosterItem.getItemMeta();
                        if (remaining <= 0) {
                            mysql.saveCountdown(player.getUniqueId(), boosterName, 0);
                            List<String> defaultLore = BoosterMain.getInstance().getConfig().getStringList("GUI." + boosterName + ".Lore");
                            if (defaultLore != null && !defaultLore.isEmpty()) {
                                for (int i = 0; i < defaultLore.size(); i++) {
                                    if (defaultLore.get(i).contains("%anzahl%")) {
                                        int amount = mysql.getAmount(player.getUniqueId().toString(), boosterName);
                                        defaultLore.set(i, ChatColor.translateAlternateColorCodes('&', defaultLore.get(i).replace("%anzahl%", String.valueOf(amount))));
                                    } else {
                                        defaultLore.set(i, ChatColor.translateAlternateColorCodes('&', defaultLore.get(i)));
                                    }
                                }
                                meta.setLore(defaultLore);
                            }
                            this.cancel();
                        } else {
                            String lore = getColoredString(BoosterMain.getInstance().getConfig().getString(boosterName + "VerbliebeneZeit"));
                            if (lore != null) {
                                lore = lore.replace("%time%", String.valueOf(remaining));
                                meta.setLore(Collections.singletonList(lore));
                            }
                            mysql.saveCountdown(player.getUniqueId(), boosterName, remaining - 1);
                        }
                        boosterItem.setItemMeta(meta);
                    }
                }.runTaskTimer(mysql.getPlugin(), 0, 30);
            }
        }
    }

    private String getColoredString(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }
}