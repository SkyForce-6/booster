package me.booster;

import me.booster.ApiUsage.BoosterNpc;
import me.booster.ApiUsage.NpcClickListener;
import me.booster.ApiUsage.VaultGUI;
import me.booster.ApiUsage.VaultGUIListener;
import me.booster.GUI.MainMenu;
import me.booster.GUI.MenuListener;
import me.booster.GUI.Menuwahl;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.SQLException;

public final class BoosterMain extends JavaPlugin {
    private static Economy econ = null;

    public static boolean setupEconomy() {
        if (Bukkit.getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = Bukkit.getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }

    public static Economy getEconomy() {
        return econ;
    }
    private static BoosterMain instance;
    private MySQL mysql;

    private static VaultGUI vaultGUI;

    public static VaultGUI getVaultGUI() {
        return vaultGUI;
    }

    public static BoosterMain getInstance() {
        return instance;
    }

    public MySQL getMySQL() {
        return mysql;
    }

    @Override
    public void onEnable() {

        this.saveDefaultConfig();

        instance = this;
        mysql = new MySQL(this);
        mysql.connect();
        mysql.addAmountColumn();

        if (setupEconomy()) {
            Economy economy = getEconomy();
            vaultGUI = new VaultGUI(economy);
        }

        this.getCommand("booster").setExecutor(new Commands(mysql, this));
        MenuListener menuListener = new MenuListener(this);
        getServer().getPluginManager().registerEvents(menuListener, this);
        this.getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);
        this.getServer().getPluginManager().registerEvents(new PluginEnableListener(this), this);
        this.getServer().getPluginManager().registerEvents(new PlayerJumpListener(this), this);

        this.getServer().getPluginManager().registerEvents(new Menuwahl(mysql, this), this);

        getServer().getPluginManager().registerEvents(new VaultGUIListener(), this);

        MainMenu mainMenu = new MainMenu(mysql);
        getServer().getPluginManager().registerEvents(new NpcClickListener(mysql), this);
        this.getCommand("Boosternpc").setExecutor(new BoosterNpc());

    }

    @Override
    public void onDisable() {
        if (mysql.isConnected()) {
            try {
                mysql.getConnection().close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}