package me.booster.ApiUsage;

import me.booster.BoosterMain;
import me.booster.GUI.Menuwahl;
import me.booster.MySQL;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class NpcClickListener implements Listener {

    private final MySQL mysql;

    public NpcClickListener(MySQL mysql) {
        this.mysql = mysql;
    }


    @EventHandler
    public void onRightClick(NPCRightClickEvent event) {
        String npcName = event.getNPC().getName();
        String configName = BoosterMain.getInstance().getConfig().getString("NPCName").replaceAll("&[a-f0-9k-or]", "");
        if (!npcName.equals(configName)) {
            return;
        }
        Menuwahl menuwahl = new Menuwahl(mysql, BoosterMain.getInstance());
        menuwahl.openMenu(event.getClicker());
    }

}