package me.booster.ApiUsage;

import me.booster.BoosterMain;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;
import java.util.stream.Collectors;

public class VaultGUI {


    private final Economy economy;
    private final Inventory inventory;

    public Inventory getInventory() {
        return this.inventory;
    }

    public Economy getEconomy() {
        return this.economy;
    }

    public VaultGUI(Economy economy) {
        this.economy = economy;
        this.inventory = createMenu();
    }

    public void openMenu(Player player) {
        player.openInventory(this.inventory);
    }

    private Inventory createMenu() {
        String menuName = getColoredString(BoosterMain.getInstance().getConfig().getString("BoosterShopName"));
        Inventory menu = Bukkit.createInventory(null, 9, menuName);

        createMenuItem(menu, "SpeedBooster");
        createMenuItem(menu, "FlyBooster");
        createMenuItem(menu, "JumpBooster");
        createMenuItem(menu, "BoosterBombe");

        createBackItem(menu, 8);


        return menu;
    }

    private void createBackItem(Inventory menu, int slot) {
        String itemName = BoosterMain.getInstance().getConfig().getString("BoosterShopItem.Back.Item");
        if (itemName != null) {
            Material material = Material.getMaterial(itemName);
            if (material != null) {
                ItemStack backItem = new ItemStack(material);
                ItemMeta meta = backItem.getItemMeta();

                String displayName = BoosterMain.getInstance().getConfig().getString("BoosterShopItem.Back.Name");
                if (displayName != null) {
                    meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', displayName));
                }
                List<String> lore = BoosterMain.getInstance().getConfig().getStringList("BoosterShopItem.Back.Lore");
                if (lore != null && !lore.isEmpty()) {
                    for (int i = 0; i < lore.size(); i++) {
                        lore.set(i, ChatColor.translateAlternateColorCodes('&', lore.get(i)));
                    }
                    meta.setLore(lore);
                }
                backItem.setItemMeta(meta);

                menu.setItem(slot, backItem);
            }
        }
    }

    private void createMenuItem(Inventory menu, String boosterName) {
        String path = "BoosterShopItem." + boosterName;
        String itemName = getColoredString(BoosterMain.getInstance().getConfig().getString(path + ".Name"));
        Material itemMaterial = Material.getMaterial(BoosterMain.getInstance().getConfig().getString(path + ".Item"));
        int slot = BoosterMain.getInstance().getConfig().getInt(path + ".Slot");
        double price = BoosterMain.getInstance().getConfig().getDouble(path + ".Preis");
        List<String> lore = BoosterMain.getInstance().getConfig().getStringList(path + ".Lore");


        lore = lore.stream()
                .map(line -> line.replace("%preis%", String.valueOf(price)))
                .collect(Collectors.toList());

        ItemStack boosterItem = new ItemStack(itemMaterial);
        ItemMeta meta = boosterItem.getItemMeta();

        meta.setDisplayName(itemName);
        meta.setLore(lore.stream().map(this::getColoredString).collect(Collectors.toList()));

        boosterItem.setItemMeta(meta);
        menu.setItem(slot, boosterItem);
    }

    private String getColoredString(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }

    public void onMenuClick(Player player, ItemStack clickedItem) {
        if (clickedItem != null && clickedItem.hasItemMeta()) {
            String boosterName = ChatColor.stripColor(clickedItem.getItemMeta().getDisplayName());
            List<String> lore = clickedItem.getItemMeta().getLore();

            if (lore != null && !lore.isEmpty()) {
                String priceLine = ChatColor.stripColor(lore.get(0));
                double price = Double.parseDouble(priceLine.split(": ")[1]);

                if (economy.has(player, price)) {
                    economy.withdrawPlayer(player, price);
                    player.sendMessage(ChatColor.GREEN + "You have purchased the " + boosterName + "!");
                } else {
                    player.sendMessage(ChatColor.RED + "You do not have enough money to purchase this item!");
                }
            }
        }
    }
}