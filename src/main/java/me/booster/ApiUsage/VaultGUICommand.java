package me.booster.ApiUsage;

import me.booster.BoosterMain;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class VaultGUICommand implements CommandExecutor {

    private VaultGUI vaultGUI;

    public VaultGUICommand() {
        if (BoosterMain.setupEconomy()) {
            Economy economy = BoosterMain.getEconomy();
            this.vaultGUI = new VaultGUI(economy);
        }
    }


        @Override
        public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
            if (sender instanceof Player) {
                Player player = (Player) sender;
                VaultGUI vaultGUI = BoosterMain.getVaultGUI();
                if (vaultGUI != null) {
                    vaultGUI.openMenu(player);
                }
                return true;
            }
            return false;
        }
}
