package me.booster.ApiUsage;

import me.booster.BoosterMain;
import me.booster.MySQL;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class VaultGUIListener implements Listener {


    private final VaultGUI vaultGUI;

    public VaultGUIListener() {
        this.vaultGUI = BoosterMain.getVaultGUI();
    }




    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {

        if (event.getInventory().equals(vaultGUI.getInventory())) {
            event.setCancelled(true);
            ItemStack clickedItem = event.getCurrentItem();

            if (clickedItem != null && clickedItem.hasItemMeta()) {
                String boosterName = ChatColor.stripColor(clickedItem.getItemMeta().getDisplayName());
                String path = "BoosterShopItem." + boosterName + ".Preis";
                double price = BoosterMain.getInstance().getConfig().getDouble(path);
                List<String> lore = clickedItem.getItemMeta().getLore();

                if (lore != null && !lore.isEmpty()) {
                        Player player = (Player) event.getWhoClicked();

                        if (vaultGUI.getEconomy().has(player, price)) {
                            vaultGUI.getEconomy().withdrawPlayer(player, price);


                            if (boosterName.equals("SpeedBooster")) {

                                MySQL mysql = BoosterMain.getInstance().getMySQL();
                                mysql.incrementAmount(player.getUniqueId().toString(), "SpeedBooster");
                                player.sendMessage(getColoredString(BoosterMain.getInstance().getConfig().getString("SpeedBoosterGekauft")));

                            } else if (boosterName.equals("FlyBooster")) {

                                MySQL mysql = BoosterMain.getInstance().getMySQL();
                                mysql.incrementAmount(player.getUniqueId().toString(), "FlyBooster");
                                player.sendMessage(getColoredString(BoosterMain.getInstance().getConfig().getString("FlyBoosterGekauft")));

                            } else if (boosterName.equals("JumpBooster")) {

                                MySQL mysql = BoosterMain.getInstance().getMySQL();
                                mysql.incrementAmount(player.getUniqueId().toString(), "JumpBooster");
                                player.sendMessage(getColoredString(BoosterMain.getInstance().getConfig().getString("JumpBoosterGekauft")));

                            } else if (boosterName.equals("BoosterBombe")) {

                                MySQL mysql = BoosterMain.getInstance().getMySQL();
                                mysql.incrementAmount(player.getUniqueId().toString(), "BoosterBombe");
                                player.sendMessage(getColoredString(BoosterMain.getInstance().getConfig().getString("BoosterBombeGekauft")));

                            }

                        } else {
                            player.closeInventory();
                            player.sendMessage(getColoredString(BoosterMain.getInstance().getConfig().getString("NichtGenugGeld")));
                        }
                }
            }
        }
    }


    private String getColoredString(String string) {
        return ChatColor.translateAlternateColorCodes('&', string);
    }


}